package com.db.bankapp;

import com.db.bankapp.domain.bank.Account;
import com.db.bankapp.domain.bank.SavingAccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Created by Student on 21.04.2015.
 */
public class IODemo {
    static ArrayList<? super Account> l = new ArrayList<>();
    public static void main(String[] args) throws IOException, InterruptedException {
        OurThread tr = new OurThread();
        l.add(new SavingAccount(4.0));
        Thread.currentThread().setName("MAIN SUPER THREAD SEE ME");
        tr.setParent(Thread.currentThread());
        Thread t = new Thread(tr);
        t.start();
        List<? extends Collection> l = new ArrayList<ArrayList>();
        tr.setNeedStop();

        while(t.isAlive()) {}
    }
}

class OurThread implements Runnable {
    Thread parent;

    public void setParent(Thread parent) {
        this.parent = parent;
    }

    private boolean needStop = false;

    public synchronized boolean isNeedStop() {
        return needStop;
    }

    public void setNeedStop() {
        this.needStop = true;
    }

    @Override
    public void run() {

        while(!isNeedStop()) {

        }
    }
}