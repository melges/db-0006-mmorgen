package com.db.bankapp.service.bank.network;

import com.db.bankapp.domain.bank.Bank;
import com.db.bankapp.domain.bank.Client;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ClientWorker implements Runnable {
    final static Logger logger = Logger.getLogger(ClientWorker.class);

    private Socket workerSocket;

    private Bank workBank;

    private ObjectInputStream reader;

    private ObjectOutputStream writer;

    public ClientWorker(Socket workSocket, Bank bank) throws IOException {
        workerSocket = workSocket;
        workBank = bank;
    }

    protected void performAddClient() throws IOException {
        try {
            Client client = (Client) reader.readObject();
            workBank.addClient(client);
            writer.writeObject("Added");
        } catch (ClassNotFoundException | ClassCastException e) {
            writer.writeObject("Invalid class of data");
        }
    }

    @Override
    public void run() {
        Thread.currentThread().setName(String.format("Network worker thread for %s",
                workerSocket.getInetAddress().getCanonicalHostName()));

        try(Socket s = workerSocket;
            ObjectInputStream reader = new ObjectInputStream(workerSocket.getInputStream());
            ObjectOutputStream writer = new ObjectOutputStream(workerSocket.getOutputStream())) {

            // Make streams global for class
            this.reader = reader;
            this.writer = writer;

            while(workerSocket.isConnected()) {
                String command = (String) reader.readObject();
                switch(command) {
                    case "print":
                        writer.writeObject(workBank.detailInfo());
                        break;
                    case "addClient":
                        performAddClient();
                        break;
                    default:
                        writer.writeObject("Unsupported operation");
                }
            }
        } catch (IOException e) {
            logger.warn(String.format("Network error while work with %s: %s",
                    workerSocket.getInetAddress().getHostAddress(), e.getMessage()));
        } catch (ClassNotFoundException e) {
            logger.error(String.format("Client %s tries to send unsupported class: %s",
                    workerSocket.getInetAddress().getHostAddress(), e.getMessage()));
        } catch (ClassCastException e) {
            logger.error(String.format("Client %s send invalid class, or in invalid order: %s",
                    workerSocket.getInetAddress().getHostAddress(), e.getMessage()));
        }
    }
}
