package com.db.bankapp.service.bank;

import com.db.bankapp.domain.bank.*;

import java.io.*;
import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Student on 21.04.2015.
 */
public class BankDataLoaderService {
    File feedFile;

    public BankDataLoaderService(String fileName) {
        this(new File(fileName));
    }

    BankDataLoaderService(File feedFile) {
        this.feedFile = feedFile;
    }

    public Bank loadBank() throws IOException, ClassNotFoundException {
        ObjectInputStream stream = new ObjectInputStream(new FileInputStream(feedFile));
        return (Bank) stream.readObject();
    }

    public void saveBank(Bank bank) throws IOException {
        try (ObjectOutputStream writer = new ObjectOutputStream(new FileOutputStream(feedFile))) {
                writer.writeObject(bank);
        }
    }
}
