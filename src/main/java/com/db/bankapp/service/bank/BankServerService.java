package com.db.bankapp.service.bank;

import com.db.bankapp.domain.bank.Bank;
import com.db.bankapp.service.bank.network.ClientWorker;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Student on 22.04.2015.
 */
public class BankServerService {
    final static int DEFAULT_PORT = 8001;

    final static Logger logger = Logger.getLogger(BankServerService.class);

    private volatile boolean continueWork = true;

    private boolean exitWithError = false;

    Thread managerWorker;

    ServerSocket listenSocket;

    Set<ClientWorker> workers = new HashSet<>();

    ExecutorService threadPool;

    public BankServerService(Bank bank, int port) throws IOException {
        listenSocket = new ServerSocket(port);
        threadPool = Executors.newCachedThreadPool();
        managerWorker = new Thread(() -> { // Implement runnable
            Thread.currentThread().setName("Network acceptor thread");
            try {
                do {
                    Socket workerSocket = listenSocket.accept();
                    ClientWorker worker = new ClientWorker(workerSocket, bank);
                    threadPool.execute(worker);

                    workers.add(worker);
                } while(continueWork && !Thread.interrupted());
            } catch (IOException e) {
                logger.error("Problems ");
                exitWithError = true;
            }
        });
        managerWorker.start();
    }

    public BankServerService(Bank bank) throws IOException {
        this(bank, DEFAULT_PORT);
    }

    public boolean join() throws InterruptedException {
        managerWorker.join();
        return !exitWithError;
    }

    public void stopAcceptNewClients() {
        try(ServerSocket s = listenSocket) {
            continueWork = false;
            threadPool.shutdown();
        } catch (IOException ignored) {
            logger.warn("IO problems on close");
        }
    }

}
