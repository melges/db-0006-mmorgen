package com.db.bankapp.service.bank;

import com.db.bankapp.domain.bank.Email;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Student on 23.04.2015.
 */
public class EmailNotificationService {
    private Queue emailQueue = new Queue();

    Thread ourThread;

    public EmailNotificationService() {
        ourThread = new Thread(() -> {
            try {
                while(!Thread.interrupted())
                    System.out.println(emailQueue.pop());
            } catch (InterruptedException e) {
                System.out.println("Email service interrupted, cancel sending email");
            }
        });

        ourThread.setName("Email notification service" + ourThread.getId());
        ourThread.start();
    }

    class Queue {
        private final List<Email> queue = new LinkedList<>();

        public void push(Email email) {
            synchronized (queue) {
                queue.add(email);
                queue.notify();
            }
        }

        public Email pop() throws InterruptedException {
            synchronized (queue) {
                if(queue.isEmpty())
                    queue.wait();
                return queue.remove(0);
            }
        }
    }

    public void sendNotification(Email email) {
        emailQueue.push(email);
    }

    public void stopNotification() {
        ourThread.interrupt();
    }
}
