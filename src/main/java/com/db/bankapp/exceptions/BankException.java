package com.db.bankapp.exceptions;

import com.db.bankapp.domain.bank.Account;

public class BankException extends Exception {
    public BankException() {
        super();
    }

    public BankException(String message) {
        super(message);
    }
}
