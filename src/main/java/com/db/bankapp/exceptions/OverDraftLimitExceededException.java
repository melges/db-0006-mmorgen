package com.db.bankapp.exceptions;

import com.db.bankapp.domain.bank.Account;

/**
 * Created by Student on 20.04.2015.
 */
public class OverDraftLimitExceededException extends NotEnoughFundsException {
    private Account causedAccount;

    public OverDraftLimitExceededException(double amount) {
        super(amount);
    }

    public OverDraftLimitExceededException(String message, double amount) {
        super(message, amount);
    }

    public OverDraftLimitExceededException(String message, double amount, Account causedAccount) {
        this(message, amount);
        this.causedAccount = causedAccount;
    }

    public OverDraftLimitExceededException(Account causedAccount, double amount) {
        this(amount);
        this.causedAccount = causedAccount;
    }

    public Account getCausedAccount() {
        return causedAccount;
    }
}
