package com.db.bankapp.exceptions;

import com.db.bankapp.domain.bank.Account;

/**
 * Created by Student on 20.04.2015.
 */
public class NotEnoughFundsException extends BankException {
    private Account account;

    private double amount;

    public NotEnoughFundsException(double amount) {
        this(String.format("Not enough money: need more %.2f", amount), amount);
    }

    public NotEnoughFundsException(String message, double amount) {
        super(message);
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
