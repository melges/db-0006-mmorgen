package com.db.bankapp.domain.bank;

import com.db.bankapp.exceptions.BankException;

public interface Account {
    int getId();

    double getBalance();

    void deposit(double sum);

    void withdraw(double sum) throws BankException;

    double maximumAmountToWithdraw();

    long decimalValue();
}
