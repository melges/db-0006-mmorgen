package com.db.bankapp.domain.bank;

import com.db.bankapp.service.bank.EmailNotificationService;

import java.io.Serializable;
import java.util.*;

public class Bank implements Serializable {
    private transient List<ClientRegistrationListener> listeners = new ArrayList<>();

    private HashSet<Client> clients = new HashSet<>();

    private transient EmailNotificationService emailService = new EmailNotificationService();

    public Bank() {
        listeners.add(
                (Client client) -> System.out.printf("%d: %s", System.currentTimeMillis(), client)
        );

        listeners.add(new EmailNotificationListener(emailService));
    }

    public Bank(ClientRegistrationListener... initialListeners) {
        this();
        listeners.addAll(Arrays.asList(initialListeners));
    }

    public static class EmailNotificationListener implements ClientRegistrationListener {
        private EmailNotificationService emailService;

        public EmailNotificationListener(EmailNotificationService emailService) {
            this.emailService = emailService;
        }

        @Override
        public void onClientAdded(Client client) {
            emailService.sendNotification(new Email(client, client.getClientSalutation() + "@db.com"));
        }
    }

    public static class PrintClientListener implements ClientRegistrationListener {
        @Override
        public void onClientAdded(Client client) {
            System.out.println(client);
        }
    }

    public void addListener(ClientRegistrationListener listener) {
        listeners.add(listener);
    }

    public void removeListener(int index) {
        listeners.remove(index);
    }

    public String detailInfo() {
        StringBuilder builder = new StringBuilder("Full bank info\n");
        builder.append("Clients number:");
        builder.append(getClients().size());
        builder.append("\n");
        for(Client client : getClients()) {
            builder.append('\n');
            builder.append(client.toString());
        }

        return builder.toString();
    }

    public Set<Client> getClients() {
        return clients;
    }

    public void addClient(Client client) {
        clients.add(client);
        for(ClientRegistrationListener listener : listeners)
            listener.onClientAdded(client);
    }

    public void setClients(Collection<Client> clients) {
        this.clients = new HashSet<>(clients);
    }

    public void stopSpamming() {
        emailService.stopNotification();
    }
}
