package com.db.bankapp.domain.bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Client implements Serializable {
    public enum Gender {
        MALE("Mr."), FEMALE("Ms.");

        private String salutation;

        Gender(String salutation) {
            this.salutation = salutation;
        }

        @Override
        public String toString() {
            return salutation;
        }
    }

    private Gender gender;

    private String name;

    public Client(Gender gender, String name) {
        this.gender = gender;
        this.name = name;
    }

    List<Account> accounts = new ArrayList<>();

    public Account[] getAccounts() {
        return accounts.toArray(new Account[accounts.size()]);
    }

    public void addAccount(Account account) {
        if(accounts.contains(account))
            return; // If already contain the client, simply return
        accounts.add(account);
    }

    public String getClientSalutation() {
        return String.format("%s %s", gender.toString(), name);
    }

    public Gender getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Client))
            return false;

        Client client = (Client) o;

        return gender == client.gender && name.equals(client.name);
    }

    @Override
    public int hashCode() {
        int result = gender.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder(getClientSalutation());
        stringBuilder.append(" with following:\n");
        for(Account account : accounts) {
            stringBuilder.append('\t');
            stringBuilder.append(account);
            stringBuilder.append('\n');
        }

        return stringBuilder.toString();
    }
}
