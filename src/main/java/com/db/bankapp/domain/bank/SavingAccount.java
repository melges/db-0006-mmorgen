package com.db.bankapp.domain.bank;
import com.db.bankapp.exceptions.NotEnoughFundsException;

import java.io.Serializable;

public class SavingAccount implements Account, Serializable {
    private static int previousId = 0;

    private int id;

    //Общую для Saving и Checking Accounts логику, равно как и общие переменные (например - balance) рекомендуется вынести в абстрактный
    //суперкласс во избежание дублирования однотипной логики в нескольких местах.
    private double balance;

    public SavingAccount(double initialBalance) {
        this.balance = initialBalance;
        id = ++previousId;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public void deposit(double sum) {
        if(sum < 0)
            throw new IllegalArgumentException("We can't deposit less than zero");
        balance += sum;
    }

    @Override
    public void withdraw(double sum) throws NotEnoughFundsException {
        if(sum < 0)
            throw new IllegalArgumentException("We can't withdraw less than zero");
        if(sum <= balance && balance > 0)
            balance -= sum;
        else
            throw new NotEnoughFundsException(-(balance - sum));
    }

    @Override
    public double maximumAmountToWithdraw() {
        return balance;
    }

    @Override
    public long decimalValue() {
        return Math.round(balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SavingAccount)) return false;

        SavingAccount that = (SavingAccount) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("Saving account with id %s and balance %.2f", id, balance);
    }
}
