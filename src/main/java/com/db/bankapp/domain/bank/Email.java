package com.db.bankapp.domain.bank;

/**
 * Created by Student on 23.04.2015.
 */
public class Email {
    private Client client;

    private String email;

    public Email(Client client, String email) {
        this.client = client;
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("Email %s on %s", client.getName(), email);
    }
}
