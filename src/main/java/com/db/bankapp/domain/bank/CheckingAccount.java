package com.db.bankapp.domain.bank;

import com.db.bankapp.exceptions.OverDraftLimitExceededException;

import java.io.Serializable;

public class CheckingAccount implements Account, Serializable {
    private static int previousId = 0;

    private Integer id;

     //То же, что для SavingAccount - общую для Saving и Checking Accounts логику (getBalance, deposit, maximumAmountToWithdraw, ...), 
     //равно как и общие переменные (например - balance) рекомендуется вынести в абстрактный
     //суперкласс во избежание дублирования однотипной логики в нескольких местах.
    private double balance;

    private double overdraft;

    public CheckingAccount(double initialBalance, double overdraft) {
        if(initialBalance < 0 || overdraft < 0)
            throw new IllegalArgumentException("Initial balance and overdraft must be greater then zero");
        balance = initialBalance;
        this.overdraft = overdraft;
        id = ++previousId;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public void deposit(double sum) {
        assert overdraft > 0 : balance + overdraft > 0;
        if(sum < 0)
            throw new IllegalArgumentException("We can't deposit less than zero");
        balance += sum;
    }

    @Override
    public void withdraw(double sum) throws OverDraftLimitExceededException {
        assert overdraft > 0 : "Overdraft must be above zero";
        assert balance + overdraft > 0 : "Balance + overdraft must be greater than zero";
        if(sum < 0)
            throw new IllegalArgumentException("We can't withdraw less than zero");
        if(balance - sum > -overdraft)
            balance -= sum;
        else
            throw new OverDraftLimitExceededException(this, -(balance + overdraft - sum));
    }

    @Override
    public double maximumAmountToWithdraw() {
        assert overdraft > 0 : "Overdraft must be above zero";
        assert balance + overdraft > 0 : "Balance + overdraft must be greater than zero";
        return balance + overdraft;
    }

    @Override
    public long decimalValue() {
        return Math.round(balance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CheckingAccount))
            return false;

        CheckingAccount that = (CheckingAccount) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("Checking account with id %d, balance %.2f and max overdraft %.2f",
                id, balance, overdraft);
    }
}
