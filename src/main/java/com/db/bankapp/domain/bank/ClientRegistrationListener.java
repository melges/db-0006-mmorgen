package com.db.bankapp.domain.bank;

import java.io.Serializable;

/**
* Created by Student on 17.04.2015.
*/
public interface ClientRegistrationListener extends Serializable {
    void onClientAdded(Client client);
}
