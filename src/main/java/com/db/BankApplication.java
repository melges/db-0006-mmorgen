package com.db;

import com.db.bankapp.domain.bank.Bank;
import com.db.bankapp.domain.bank.CheckingAccount;
import com.db.bankapp.domain.bank.Client;
import com.db.bankapp.domain.bank.SavingAccount;
import com.db.bankapp.exceptions.BankException;
import com.db.bankapp.exceptions.NotEnoughFundsException;
import com.db.bankapp.exceptions.OverDraftLimitExceededException;
import com.db.bankapp.service.bank.BankServerService;
import com.db.bankapp.service.bank.BankDataLoaderService;
import com.db.bankapp.service.bank.EmailNotificationService;
import org.apache.log4j.Logger;

public class BankApplication {
    private static final Logger logger = Logger.getLogger(BankApplication.class);

    public static void main(String[] args) throws Exception {
        Bank bank = new Bank(new Bank.PrintClientListener());

        Client firstClient = new Client(Client.Gender.MALE, "Morgen Matvey");
        firstClient.addAccount(new SavingAccount(24.45));
        firstClient.addAccount(new CheckingAccount(28.5, 5));
        Client secondClient = new Client(Client.Gender.FEMALE, "Almuhamedova Ekaterina");
        secondClient.addAccount(new CheckingAccount(294.45, 50.0));

        assert firstClient.equals(firstClient) : "Equals method invalid";
        assert !secondClient.equals(firstClient) : "Equals method invalid";

        bank.addClient(firstClient);
        bank.addClient(secondClient);

        try {
            modifyBank(bank);
            printBalance(bank);
            BankDataLoaderService loader = new BankDataLoaderService(args[0]);
            loader.saveBank(bank);
            System.out.println("\n\nBANK SAVED!\n\n");
            printBalance(loader.loadBank());

            BankServerService server = new BankServerService(bank);

            server.join();

            bank.stopSpamming();

            loader.saveBank(bank);
        } catch (OverDraftLimitExceededException e) {
            logger.warn(String.format("%s on account %s", e.getMessage(), e.getCausedAccount()));
        } catch (NotEnoughFundsException e) {
            logger.warn(e.getMessage());
        } catch (BankException e) {
            logger.error("Unexpected error", e);
        }
    }

    public static void modifyBank(Bank bank) throws BankException {
        for(Client client : bank.getClients()) {
            client.getAccounts()[0].deposit(5.4);
            client.getAccounts()[0].withdraw(5.6);
        }
    }

    public static void printBalance(Bank bank) {
        bank.getClients().forEach(System.out::println);
    }
}
