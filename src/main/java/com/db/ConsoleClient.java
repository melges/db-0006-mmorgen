package com.db;

import com.db.bankapp.domain.bank.Client;

import java.io.*;
import java.net.Socket;

/**
 * Created by Student on 22.04.2015.
 */
public class ConsoleClient {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
        Socket socket = new Socket("localhost", 8001);
        ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
        String command = null;

        while(!"exit".equals(command = console.readLine())) {
            if("addClient".equals(command)) {
                Client client = new Client(Client.Gender.valueOf(console.readLine()), console.readLine());
                out.writeObject(command);
                out.writeObject(client);
                continue;
            }
            out.writeObject(command);
            System.out.println(in.readObject().toString());
        }
    }
}
